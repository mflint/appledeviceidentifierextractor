// swift-tools-version: 5.10
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
	name: "AppleDeviceIdentifierExtractor",
	platforms: [
		.macOS(.v13),
	],
	dependencies: [
		// Dependencies declare other packages that this package depends on.
		// .package(url: /* package url */, from: "1.0.0"),
		.package(url: "https://github.com/apple/swift-argument-parser.git", .upToNextMajor(from: "1.0.0")),
		.package(url: "https://gitlab.com/mflint/AppleDeviceIdentifier", branch: "main"),
	],
	targets: [
		.executableTarget(
			name: "extractor",
			dependencies: [
				.product(name: "ArgumentParser", package: "swift-argument-parser"),
				.product(name: "AppleDeviceIdentifier", package: "AppleDeviceIdentifier"),
		])
	]
)
