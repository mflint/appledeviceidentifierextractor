import Foundation
import ArgumentParser
import SQLite3
import AppleDeviceIdentifier

extension Sequence {
	func sorted<T: Comparable>(by keyPath: KeyPath<Element, T>) -> [Element] {
		return sorted { a, b in
			return a[keyPath: keyPath] < b[keyPath: keyPath]
		}
	}
}

private enum Error: Swift.Error {
	case noXcode
	case badURL
	case databaseOpenFailure
	case selectStatementPreparationFailure
	case selectStatementFinalizeFailure
	case modelParseFailure(String)
	case unknownDeviceType(String)
}

private enum DeviceType: Comparable {
	case iPhone
	case iPod
	case iPad
	case AppleTV
	case Watch
	case RealityDevice

	// ignored
	case MacFamily
	case RealityFamily

	init?(rawValue: String) {
		if rawValue == "iPhone" {
			self = .iPhone
		} else if rawValue == "iPod" {
			self = .iPod
		} else if rawValue == "iPad" {
			self = .iPad
		} else if rawValue == "AppleTV" {
			self = .AppleTV
		} else if rawValue == "Watch" {
			self = .Watch
		} else if rawValue == "RealityDevice" {
			self = .RealityDevice
		} else if rawValue == "MacFamily" {
			self = .MacFamily
		} else if rawValue == "RealityFamily" {
			self = .RealityFamily
		} else {
			return nil
		}
	}
}

private struct ModelVersion {
	let deviceType: DeviceType
	let major: Int
	let minor: Int

	init(type: String) throws {
		guard let position = type.firstIndex(where: { c in c.isNumber }) else {
			throw Error.modelParseFailure(type)
		}

		let sanitised = if let dashPosition = type.firstIndex(of: "-") {
			String(type.prefix(upTo: dashPosition))
		} else {
			type
		}

		guard let deviceType = DeviceType(rawValue: String(sanitised.prefix(upTo: position))) else {
			throw Error.unknownDeviceType(type)
		}
		let majorMinor = sanitised.suffix(from: position)
			.split(separator: ",")
		guard majorMinor.count == 2 else {
			throw Error.modelParseFailure(type)
		}

		guard let major = Int(majorMinor[0]),
			  let minor = Int(majorMinor[1]) else {
			throw Error.modelParseFailure(type)
		}

		self.deviceType = deviceType
		self.major = major
		self.minor = minor
	}
}

extension ModelVersion: CustomStringConvertible {
	var description: String { "\(deviceType)\(major),\(minor)" }
}

extension ModelVersion: Comparable {
	static func < (lhs: ModelVersion, rhs: ModelVersion) -> Bool {
		if lhs.deviceType != rhs.deviceType {
			return lhs.deviceType < rhs.deviceType
		} else if lhs.major != rhs.major {
			return lhs.major < rhs.major
		} else {
			return lhs.minor < rhs.minor
		}
	}
}

@main
struct Extractor: ParsableCommand {
	private struct Model {
		let type: ModelVersion
		let description: String

		init(type: String, description: String) throws {
			self.type = try ModelVersion(type: type)
			self.description = description
		}
	}

	@Flag(name: NameSpecification.customLong("new"),
		  help: "Show only devices which are not in the AppleDeviceIdentifier Swift package.")
	private var onlyNew = false

	func run() throws {
		let xcode = try self.latestXcode()
		var models = [Model]()
		try models.append(contentsOf: self.devices(xcode: xcode,
												   platform: "AppleTVOS"))
		try models.append(contentsOf: self.devices(xcode: xcode,
												   platform: "iPhoneOS"))
		try models.append(contentsOf: self.devices(xcode: xcode,
												   platform: "WatchOS"))
		try models.append(contentsOf: self.devices(xcode: xcode,
												   platform: "XROS"))

		for model in models
			.sorted(by: \.type)
			.filter({ model in
				if [
					DeviceType.MacFamily,
					DeviceType.RealityFamily
				].contains(model.type.deviceType) {
					return false
				}

				return !self.onlyNew || AppleDevice.models[model.type.description] == nil
			}) {
			print("\"\(model.type)\": \"\(model.description)\",")
		}
	}

	private func latestXcode() throws -> String {
		let contents = try FileManager.default.contentsOfDirectory(atPath: "/Applications/")
		guard let xcode = contents
			.filter({ application in application.hasPrefix("Xcode-") })
			.sorted()
			.last else {
				throw Error.noXcode
			}
		return xcode
	}

	private func devices(xcode: String, platform: String) throws -> [Model] {
		guard let dbPath = URL(string: "/Applications/\(xcode)/Contents/Developer/Platforms/\(platform).platform/usr/standalone/device_traits.db") else {
			throw Error.badURL
		}

		var db: OpaquePointer?
		defer {
			sqlite3_close(db)
			db = nil
		}

		guard sqlite3_open(dbPath.absoluteString, &db) == SQLITE_OK,
			  let db else {
			throw Error.databaseOpenFailure
		}

		var statement: OpaquePointer?
		guard sqlite3_prepare_v2(db, "select ProductType, ProductDescription from Devices", -1, &statement, nil) == SQLITE_OK else {
			throw Error.selectStatementPreparationFailure
		}

		var models = [Model]()

		while sqlite3_step(statement) == SQLITE_ROW {
			if let cType = sqlite3_column_text(statement, 0),
			   let cDescription = sqlite3_column_text(statement, 1) {
				let type = String(cString: cType)
				let description = String(cString: cDescription)
				try models.append(Model(type: type, description: description))
			}
		}

		guard sqlite3_finalize(statement) == SQLITE_OK else {
			throw Error.selectStatementFinalizeFailure
		}

		statement = nil

		return models
	}
}
