# AppleDeviceIdentifierExtractor

## What is this?

This is a Swift commandline tool which extracts model information about Apple products from a SQLite database that is distributed with Xcode.

## Why?

I use this to update the [AppleDeviceIdentifier] Swift Package.

## How to run?

List all devices:
```
swift run extractor
```

List only devices that are not already present in the AppleDeviceIdentifier Swift package:
```
swift run extractor --new
```

This will:

* find the latest installed version of Xcode
* open the `device_traits.db` database for TVOS, iPhoneOS, WatchOS and XROS platforms
* dump the discovered devices to the console, in (almost) a Swift dictionary format


[AppleDeviceIdentifier]: https://gitlab.com/mflint/AppleDeviceIdentifier

## License

MIT
